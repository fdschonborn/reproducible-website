---
layout: default
title: Who is involved?
permalink: /projects/
order: 40
redirect_from: /who/
---

# Who is involved?

Various free software projects are working on providing reproducible builds to their users and developers via the not-for-profit Reproducible Builds project.
{: .lead}

<div class="row">
<div class="col-md-6" markdown="1">

### Core team

* [Chris Lamb](https://chris-lamb.co.uk) (`lamby`)
* [Holger Levsen](http://layer-acht.org/thinking/) (`h01ger`)
* [Mattia Rizzolo](https://mapreri.org/) (`mapreri`)
* [Vagrant Cascadian](https://www.aikidev.net/about/story/) (`vagrantc`)


</div>
<div class="col-md-6" markdown="1">

### Steering Committee

* [Allen Gunn](https://aspirationtech.org)
* [Bdale Garbee](http://gag.com/bdale/)
* [Holger Levsen](http://layer-acht.org/thinking/)
* [Keith Packard](https://keithp.com)
* [Mattia Rizzolo](https://mapreri.org/)
* [Stefano Zacchiroli](https://upsilon.cc/)

</div>
</div>

<br>

The preferred way to contact the team is to post to [our public mailing list, `rb-general`](https://lists.reproducible-builds.org/listinfo/rb-general). However, if you wish to contact the core team directly, please email [`contact@reproducible-builds.org`](mailto:contact@reproducible-builds.org).

## Sponsors

See [who is currently supporting]({{ "/sponsors" | relative_url }}) the Reproducible Builds project, and if you are interested in our work please consider [becoming a sponsor]({{ "/donate/" | relative_url }}).
{: .lead}

## Affiliated projects

<div class="projects row bg-light p-md-5 p-sm-3 pt-5 pb-5">
    {% for project in site.data.projects %}
    <div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
        <div class="card" name="{{ project.name }}">
            <a href="{{ project.url }}" name="{{ project.name }}">
                <img class="p-5 project-img" src="{{ project.logo | prepend: "/images/logos/" | relative_url }}" alt="{{ project.name }}">
            </a>
            <div class="card-body">
                <h4 class="card-title"><a href="{{ project.url }}">{{ project.name }}</a></h4>
            </div>
            <ul class="list-group list-group-flush">
                {% for resource in project.resources %}
                    <li class="list-group-item">
                        <a href="{{ resource.url }}">{{ resource.name }}</a>
                    </li>
                {% endfor %}
                {% if project.tests %}
                    <li class="list-group-item">
                        <a href="{{ project.tests }}">Continuous tests</a>
                    </li>
                {% endif %}
            </ul>
        </div>
    </div>
    {% endfor %}
</div>
